import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import 'generated/codegen_loader.g.dart';

void main() {
  runApp(
    EasyLocalization(
      child: MyApp(),
      supportedLocales: [Locale('ar', 'DZ'), Locale('en', 'US')],
      path: 'assets/languages',
      assetLoader: CodegenLoader(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Translation test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      locale: context.locale,
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('title').tr(),
            Text('clickMe').tr(),
            Text('msg').tr(args: ['Bibi', 'Flutter']),
            Text('msg_named')
                .tr(args: ['Those books'], namedArgs: {'lang': 'English'}),
            Switch(
              value: isSwitched,
              onChanged: (value) {
                setState(() {
                  isSwitched = value;
                  context.locale =
                      isSwitched ? Locale('en', 'US') : Locale('ar', 'DZ');
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
