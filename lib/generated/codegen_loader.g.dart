// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> ar_DZ = {
  "title": "السلام",
  "msg": "السلام عليكم يا {} في عالم {}",
  "msg_named": "{} مكتوبة باللغة {lang}",
  "clickMe": "إضغط هنا",
  "profile": {
    "reset_password": {
      "label": "اعادة تعين كلمة السر",
      "username": "المستخدم",
      "password": "كلمة السر"
    }
  },
  "clicked": {
    "zero": "لم تنقر بعد!",
    "one": "لقد نقرت مرة واحدة!",
    "two": "لقد قمت بالنقر مرتين!",
    "few": " لقد قمت بالنقر {} مرات!",
    "many": "لقد قمت بالنقر {} مرة!",
    "other": "{} نقرة!"
  },
  "amount": {
    "zero": "المبلغ : {}",
    "one": " المبلغ : {}",
    "two": " المبلغ : {}",
    "few": " المبلغ : {}",
    "many": " المبلغ : {}",
    "other": " المبلغ : {}"
  },
  "gender": {
    "male": " مرحبا يا رجل",
    "female": " مرحبا بك يا فتاة",
    "with_arg": {
      "male": "{} مرحبا يا رجل",
      "female": "{} مرحبا بك يا فتاة"
    }
  },
  "reset_locale": "إعادة تعيين اللغة المحفوظة"
};
static const Map<String,dynamic> ar = {
  "title": "السلام",
  "msg": "السلام عليكم يا {} في عالم {}",
  "msg_named": "{} مكتوبة باللغة {lang}",
  "clickMe": "إضغط هنا",
  "profile": {
    "reset_password": {
      "label": "اعادة تعين كلمة السر",
      "username": "المستخدم",
      "password": "كلمة السر"
    }
  },
  "clicked": {
    "zero": "لم تنقر بعد!",
    "one": "لقد نقرت مرة واحدة!",
    "two": "لقد قمت بالنقر مرتين!",
    "few": " لقد قمت بالنقر {} مرات!",
    "many": "لقد قمت بالنقر {} مرة!",
    "other": "{} نقرة!"
  },
  "amount": {
    "zero": "المبلغ : {}",
    "one": " المبلغ : {}",
    "two": " المبلغ : {}",
    "few": " المبلغ : {}",
    "many": " المبلغ : {}",
    "other": " المبلغ : {}"
  },
  "gender": {
    "male": " مرحبا يا رجل",
    "female": " مرحبا بك يا فتاة",
    "with_arg": {
      "male": "{} مرحبا يا رجل",
      "female": "{} مرحبا بك يا فتاة"
    }
  },
  "reset_locale": "إعادة تعيين اللغة المحفوظة"
};
static const Map<String,dynamic> en_US = {
  "title": "Hello",
  "msg": "Hello {} in the {} world ",
  "msg_named": "{} are written in the {lang} language",
  "clickMe": "Click me",
  "profile": {
    "reset_password": {
      "label": "Reset Password",
      "username": "Username",
      "password": "password"
    }
  },
  "clicked": {
    "zero": "You clicked {} times!",
    "one": "You clicked {} time!",
    "two": "You clicked {} times!",
    "few": "You clicked {} times!",
    "many": "You clicked {} times!",
    "other": "You clicked {} times!"
  },
  "amount": {
    "zero": "Your amount : {} ",
    "one": "Your amount : {} ",
    "two": "Your amount : {} ",
    "few": "Your amount : {} ",
    "many": "Your amount : {} ",
    "other": "Your amount : {} "
  },
  "gender": {
    "male": "Hi man ;) ",
    "female": "Hello girl :)",
    "with_arg": {
      "male": "Hi man ;) {}",
      "female": "Hello girl :) {}"
    }
  },
  "reset_locale": "Reset Saved Language"
};
static const Map<String,dynamic> en = {
  "title": "Hello",
  "msg": "Hello {} in the {} world ",
  "msg_named": "{} are written in the {lang} language",
  "clickMe": "Click me",
  "profile": {
    "reset_password": {
      "label": "Reset Password",
      "username": "Username",
      "password": "password"
    }
  },
  "clicked": {
    "zero": "You clicked {} times!",
    "one": "You clicked {} time!",
    "two": "You clicked {} times!",
    "few": "You clicked {} times!",
    "many": "You clicked {} times!",
    "other": "You clicked {} times!"
  },
  "amount": {
    "zero": "Your amount : {} ",
    "one": "Your amount : {} ",
    "two": "Your amount : {} ",
    "few": "Your amount : {} ",
    "many": "Your amount : {} ",
    "other": "Your amount : {} "
  },
  "gender": {
    "male": "Hi man ;) ",
    "female": "Hello girl :)",
    "with_arg": {
      "male": "Hi man ;) {}",
      "female": "Hello girl :) {}"
    }
  },
  "reset_locale": "Reset Saved Language"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"ar_DZ": ar_DZ, "ar": ar, "en_US": en_US, "en": en};
}
